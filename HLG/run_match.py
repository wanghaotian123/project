import args
from dataloader.matcher import load_data
from dataprocessor.matcher import get_processor
from main import main

if __name__ == "__main__":
    args = args.get_args()
    model_id = '1'

    processor = None
    if args.exp_name == 'LCQMC':  # 口语化描述的语义相似度任务（SPM）:输入是两个句子，输出是0或1。其中0代表语义不相似，1代表语义相似。
        processor = get_processor(['0', '1'])
    elif args.exp_name == 'BQ':  # 智能客服问句匹配（SPM）
        processor = get_processor(['0', '1'])
    elif args.exp_name == 'XNLI':  # 语言推断任务（NLI）:给定一个前提和假设，判断这个假设与前提是否具有蕴涵、对立、中性关系。
        processor = get_processor(['contradiction', 'entailment', 'neutral'])
    else:
        print('incorrect exp_name')
        exit(0)

    main(args, model_id, processor, load_data)
