# 中文命名实体识别

## 项目简介

本项目尝试使用了多种不同的模型（包括HMM，CRF，Bi-LSTM+CRF）来解决中文命名实体识别问题

## 项目结构

```
|-- named_entity_recognition
    |-- ckpts                                       ---保存训练好的模型
    |   |-- crf.pkl                                 ---训练好的CFR命名实体识别模型
    |   |-- hmm.pkl                                 ---训练好的HMM命名实体识别模型
    |   |-- bilstm_crf.pkl                          ---训练好的BiLSTM-CRF命名实体识别模型
    |-- models                                      ---模型的定义
    |   |-- hmm.py                                  ---HMM模型定义
    |   |-- crf.py                                  ---CRF模型定义
    |   |-- bilstm.py                               ---BiLSTM模型定义
    |   |-- bilstm_crf.py                           ---BiLSTM-CRF模型定义
    |   |-- config.py                               ---模型配置
    |   |-- util.py                                 ---定义分词器和损失计算方法
    |-- ResumeNER                                   ---ResumeNER数据集
    |   |-- dev.char.bmes                           ---验证集
    |   |-- test.char.bmes                          ---测试集
    |   |-- train.char.bmes                         ---训练集
    |-- data.py                                     ---读取数据集
    |-- evaluate.py                                 ---训练并评估模型
    |-- evaluating.py                               ---模型评估工具
    |-- main.py                                     ---调用evaluate.py中的方法对模型进行训练并保存模型
    |-- test.py                                     ---加载并评估训练好的模型
    |-- predict.py                                  ---使用训练好的模型进行中文命名实体识别
    |-- utils.py                                    ---工具类
    |-- output.txt                                  ---模型评估输出
```

## 数据集

此项目使用的数据集是ResumeNER数据集。

Resume NER是根据新浪财经网关于上市公司的高级经理人的简历摘要数据，进行筛选过滤和人工标注生成的。该数据集包含1027份简历摘要。实体标注分为人名（NAME）、国籍（CONT）、籍贯（LOC）、种族（RACE）、专业（PRO）、学位（EDU）、机构（ORD）、职称（TITLE）8个类别。

ResumeNER下载链接：[数据集下载](https://gitcode.net/weixin_39792418/named_entity_recognition/-/tree/master/ResumeNER?from_codechina=yes)

例子：

```
美 B-LOC
国 E-LOC
的 O
华 B-PER
莱 I-PER
士 E-PER

我 O
跟 O
他 O
谈 O
笑 O
风 O
生 O 
```

## 运行环境

windows10 + python3.6

`requirements.txt`

```
numpy==1.19.5
python-crfsuite==0.9.8
six==1.16.0
sklearn-crfsuite==0.3.6
tabulate==0.8.9
torch==1.10.2
tqdm==4.31.1
```

## 快速开始

**安装依赖：**

```
pip install -r requirement.txt
```

**训练模型：**

```
python main.py
```

运行`main.py`同时训练并评估三个模型，并将训练好的模型保存至`ckpts`文件夹中，评估模型将会打印出模型的精确率、召回率、F1分数值以及混淆矩阵，如果想要修改相关模型参数或者是训练参数，可以在`./models/config.py`文件中进行设置。

训练完毕之后，如果想要加载并评估模型，运行如下命令：

```
python test.py
```

**使用模型：**

```
python predict.py
```

运行`predict.py`可使用模型进行预测，其中`get_entities_result(sentences, model_name)`方法可识别出文本中的命名实体，它的第一个参数`sentences`是要进行命名实体识别的句子列表，第二个参数`model_name`是模型名称，此参数决定了调用哪个模型进行命名实体识别。

**示例：**

```python
if __name__ == '__main__':
    sentence_list = ["王浩天，男，汉族，毕业于河北工业大学，硕士学位。"]
    model_name = 'bilstm-crf' # 使用bilstm-crf模型，除以之外还有hmm和crf
    entities = get_entities_result(sentence_list, model_name)
    for item in entities:
        print(item)
```

输出：

```shell
[{'value': '王浩天', 'type': 'name', 'begin': 0, 'end': 3}, {'value': '汉族', 'type': 'race', 'begin': 6, 'end': 8}, {'value': '河北工业大学', 'type': 'organization', 'begin': 12, 'end': 18}, {'value': '硕士学位', 'type': 'degree', 'begin': 19, 'end': 23}]
```

## 运行结果

下面是三种不同的模型预测结果的准确率、召回率和F1分数：

|        |  HMM   |  CRF   | BiLSTM-CRF |
| :----: | :----: | :----: | :--------: |
| 准确率 | 91.49% | 95.43% |   95.74%   |
| 召回率 | 91.22% | 95.43% |   95.72%   |
|  F1值  | 91.30% | 95.42% |   95.70%   |

