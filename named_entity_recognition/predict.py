from data import build_corpus
from utils import load_model, extend_maps, prepocess_data_for_lstmcrf, get_word_lists

HMM_MODEL_PATH = './ckpts/hmm.pkl'
CRF_MODEL_PATH = './ckpts/crf.pkl'
BiLSTMCRF_MODEL_PATH = './ckpts/bilstm_crf.pkl'


def get_entities_result(sentences, model_name):
    _, _, word2id, tag2id = build_corpus("train")
    word_lists = get_word_lists(sentences)
    if model_name == 'hmm':
        model = load_model(HMM_MODEL_PATH)
        predict_labels = model.test(word_lists, word2id, tag2id)
    elif model_name == 'crf':
        model = load_model(CRF_MODEL_PATH)
        predict_labels = model.test(sentences)
    elif model_name == 'bilstm-crf':
        model = load_model(BiLSTMCRF_MODEL_PATH)
        model.model.bilstm.bilstm.flatten_parameters()
        crf_word2id, crf_tag2id = extend_maps(word2id, tag2id, for_crf=True)
        word_lists, _ = prepocess_data_for_lstmcrf(word_lists, word_lists, test=True)
        predict_labels, _ = model.test(word_lists, word_lists, crf_word2id, crf_tag2id)
    else:
        predict_labels = []
        raise ValueError("The parameter model_name entered is not defined!")

    entities_list = []
    map_dict = {"NAME": "name", "TITLE": "title", "EDU": "degree", "ORG": "organization", "RACE": "race",
                "PRO": "profession", "CONT": "country", "LOC": "location"}
    for i in range(len(word_lists)):
        if model_name == 'bilstm-crf':
            sentence_list = word_lists[i][:-1]
        else:
            sentence_list = word_lists[i]
        label_list = predict_labels[i]
        entities = []
        if len(sentence_list) == len(label_list):
            result = bme_data_handler(sentence_list, label_list, map_dict)
            if len(result) != 0:
                end = 0
                prefix_len = 0

                for word, label in result:
                    sen = sentences[i].lower()[end:]
                    begin = sen.find(word) + prefix_len
                    end = begin + len(word)
                    prefix_len = end
                    if begin != -1:
                        ent = dict(value=sentences[i][begin:end], type=label, begin=begin, end=end)
                        entities.append(ent)
        entities_list.append(entities)
    return entities_list


def bme_data_handler(sentence, predict_label, map_dic):
    """根据标签序列提取出实体"""
    entities = []
    # 获取初始位置实体标签
    pre_label = predict_label[0]
    # 实体词初始化
    word = ""
    for i in range(len(sentence)):
        # 记录问句当前位置词的实体标签
        current_label = predict_label[i]
        # 若当前位置的实体标签是以B开头的，说明当前位置是实体开始位置
        if current_label.startswith('B'):
            # 当前位置所属标签类别与前一位置所属标签类别不相同且实体词不为空，则说明开始记录新实体，前面的实体需要加到实体结果中
            if pre_label[2:] is not current_label[2:] and word != "":
                entities.append([word, map_dic[pre_label[2:]]])
                # 将当前实体词清空
                word = ""
            # 并将当前的词加入到实体词中
            word += sentence[i]
            # 记录当前位置标签为前一位置标签
            pre_label = current_label
        # 若当前位置的实体标签是以I开头的，说明当前位置是实体中间位置，将当前词加入到实体词中
        elif current_label.startswith('M'):
            word += sentence[i]
            pre_label = current_label
        elif current_label.startswith('E'):
            word += sentence[i]
            pre_label = current_label
            if pre_label[2:] is current_label[2:]:
                entities.append([word, map_dic[current_label[2:]]])
                # 将当前实体词清空
                word = ""
        # 若当前位置的实体标签是以O开头的，说明当前位置不是实体，需要将实体词加入到实体结果中
        elif current_label.startswith('O'):
            # 当前位置所属标签类别与前一位置所属标签类别不相同且实体词不为空，则说明开始记录新实体，前面的实体需要加到实体结果中
            if pre_label[2:] is not current_label[2:] and word != "":
                entities.append([word, map_dic[pre_label[2:]]])
            # 记录当前位置标签为前一位置标签
            pre_label = current_label
            # 并将当前的词加入到实体词中
            word = ""
        elif current_label.startswith('S'):
            word += sentence[i]
            pre_label = current_label
    # 收尾工作，遍历问句完成后，若实体刚好处于最末位置，将剩余的实体词加入到实体结果中
    if word != "":
        entities.append([word, map_dic[pre_label[2:]]])
    return entities


if __name__ == '__main__':
    sentence_list = ["王浩天，男，汉族，毕业于河北工业大学，硕士学位。"]
    model_name = 'bilstm-crf'
    entities = get_entities_result(sentence_list, model_name)
    for item in entities:
        print(item)
