# BERT-BiLSTM-CRF模型

### 【简介】使用谷歌的BERT模型在BiLSTM-CRF模型上进行预训练用于中文命名实体识别

### 项目结构

```
|-- chinese_ner
    |-- torch_ner
        |-- data                                        ---放置数据集
        |   |-- cluener                                 ---ClueNER数据集
        |   |   |-- clue2bioes.py                       ---将json格式数据转为bioes数据格式
        |   |   |-- dev.json                            ---验证集（json格式）
        |   |   |-- test.json                           ---测试集（json格式）
        |   |   |-- train.json                          ---训练集（json格式）
        |   |   |-- result                              ---放置bioes格式数据
        |   |       |-- cluener.dev.bioes               ---验证集
        |   |       |-- cluener.test.bioes              ---测试集
        |   |       |-- cluener.train.bioes             ---训练集
        |   |-- resumener                               ---ResumeNER数据集
        |       |-- dev.char.bmes                       ---验证集
        |       |-- test.char.bmes                      ---测试集
        |       |-- train.char.bmes                     ---训练集
        |-- output                                      ---模型、日志信息等
        |   |-- clue_ner                                ---训练ClueNER数据得到的模型和输出信息
        |   |   |-- 20220417160929                      ---bert预训练模型和自己训练好的NER模型
        |   |   |   |-- eval                            ---模型结果，包括F1值、准确率、召回率
        |   |-- resume_ner                              ---训练ResumeNER数据得到的模型和输出信息
        |       |-- 20220416135953                      ---bert预训练模型和自己训练好的NER模型
        |       |   |-- eval                            ---模型结果，包括F1值、准确率、召回率
        |-- source                                      ---源代码
        |   |-- config.py                               ---项目配置、模型参数
        |   |-- conlleval.py                            ---模型验证工具（在训练过程中调用）
        |   |-- logger.py                               ---项目日志配置
        |   |-- models.py                               ---bert_bilstm_crf模型的torch实现
        |   |-- ner_main.py                             ---训练主模块，包括训练、保存模型、模型验证
        |   |-- ner_predict.py                          ---使用模型进行命名实体识别
        |   |-- ner_processor.py                        ---数据预处理
        |   |-- utils.py                                ---工具包
```

### 数据集

#### 1、CLUENER2020数据集

训练集：10748        验证集：1343        测试集：1345

标签类别：地址（address），书名（book），公司（company），游戏（game），政府（goverment），电影（movie），姓名（name），组织机构（organization），职位（position），景点（scene）

CLUENER2020下载链接：[数据下载](https://www.cluebenchmarks.com/introduce.html)

例子：

```json
{"text": "浙商银行企业信贷部叶老桂博士则从另一个角度对五道门槛进行了解读。叶老桂认为，对目前国内商业银行而言，", "label": {"name": {"叶老桂": [[9, 11]]}, "company": {"浙商银行": [[0, 3]]}}}
```


原始数据格式为json格式，需要将其转为bioes格式的数据，转换后的数据格式如下：

```
浙 B-company
商 I-company
银 I-company
行 E-company
企 O
业 O
信 O
贷 O
部 O
叶 B-name
老 I-name
桂 E-name
博 O
士 O
...
```

#### 2、ResumeNER数据集

Resume NER是根据新浪财经网关于上市公司的高级经理人的简历摘要数据，进行筛选过滤和人工标注生成的。该数据集包含1027份简历摘要。实体标注分为人名（NAME）、国籍（CONT）、籍贯（LOC）、种族（RACE）、专业（PRO）、学位（EDU）、机构（ORD）、职称（TITLE）8个类别。

ResumeNER下载链接：[数据下载](https://gitcode.net/weixin_39792418/named_entity_recognition/-/tree/master/ResumeNER?from_codechina=yes)

例子：

```
美 B-LOC
国 E-LOC
的 O
华 B-PER
莱 I-PER
士 E-PER

我 O
跟 O
他 O
谈 O
笑 O
风 O
生 O 
```

### 运行环境

```
torch==1.8.0
pytorch_crf==0.7.2
numpy==1.17.0
transformers==4.9.0
tqdm==4.62.0
PyYAML==5.4.1
tensorboardX==2.4
tensorboard==2.8.0
```

### 使用方法

- **修改项目配置**

  因为本项目使用了两种数据集训练了两个模型，所以在训练和预测时都需要选择数据集和模型

  `config.py`

  ```python
      def _init_train_config(self):
          self.label_list = []
          self.use_gpu = True
          self.device = 'cpu'
          self.sep = " "
          self.dataset = "cluener"  # 此处选择数据集
  ```

  如果使用cluener数据集进行训练，就把`self.dataset`的值设置为`cluener`，如果使用resumener数据集进行训练，就把`self.dataset`设置为`resumener`

- **训练模型**

  运行`ner_main.py`

  训练模型过程中的输出日志会保存到`output`文件夹中，训练好的模型会保存到`output`文件夹下以当前时间为命名的文件夹中             

  训练时间：

  ​	CLUENER2020：约150min（gtx 1050）

  ​	RESUMENER：约100min（gtx1050）

  模型输出：

  ​	训练过程中每轮的F1值、precision、recall保存在`eval`文件夹下，此文件可用tensorboard工具打开，在虚拟环境中安装好tensorboard，将路径切换至`eval`所在文件夹，执行如下命令

  ```shell
  tensorboard --logdir eval
  ```

  然后在浏览器打开http://localhost:6006/即可查看数据，如下图

  ![image-20220418111246203](img/f1_and_loss.png)

- **预测**

  `ner_predict.py`使用模型进行预测

  ```python
  if __name__ == '__main__':
      # 使用cluener训练的模型用这个path
      clue_path = os.path.join(os.path.abspath('..'), 'output\\clue_ner\\20220417160929')
      # 使用resumener训练的模型用这个
      resume_path = os.path.join(os.path.abspath('..'), 'output\\resume_ner\\20220416135953')
      sent = "当天晚上，等到孙晓凯和王浩天等5人回到大连，已经是晚上12点。"
      # 使用cluener数据集训练出的模型进行预测
      entities = get_entities_result(sent,clue_map_dic,resume_path)
      print(entities)
  ```

  `get_entities_result(query, map_dic,model_path)`有3个参数，query为要进行实体识别的句子，map_dic为标签字典，model_path为模型保存的路径。

  若使用基于cluener数据集训练的模型进行预测，将map_dic设置为clue_map_dic，同时将path设置为cluener对应模型的保存位置。

  若使用基于resumener数据集训练的模型进行预测，将map_dic设置为resume_map_dic，同时将path设置为resumener对应模型的保存位置。
